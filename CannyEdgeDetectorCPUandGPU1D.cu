#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <sys/time.h>
//CUDA
#include <cuda.h>

#define THREADS_BLOCKS 16
#define NTHREADS 256
#define block 16


double get_time(){
	static struct timeval 	tv0;
	double time_, time;

	gettimeofday(&tv0,(struct timezone*)0);
	time_=(double)((tv0.tv_usec + (tv0.tv_sec)*1000000));
	time = time_/1000000;
	return(time);
}

unsigned char *readBMP(char *file_name, char header[54], int *w, int *h)
{
	//Se abre el fichero en modo binario para lectura
	FILE *f=fopen(file_name, "rb");
	if (!f){
		perror(file_name); exit(1);
	}

	// Cabecera archivo imagen
	//***********************************
	//Devuelve cantidad de bytes leidos
	int n=fread(header, 1, 54, f);

	//Si no lee 54 bytes es que la imagen de entrada es demasiado pequeña
	if (n!=54)
		fprintf(stderr, "Entrada muy pequeña (%d bytes)\n", n), exit(1);

	//Si los dos primeros bytes no corresponden con los caracteres BM no es un fichero BMP
	if (header[0]!='B'|| header[1]!='M')
		fprintf(stderr, "No BMP\n"), exit(1);

	//El tamaño de la imagen es el valor de la posicion 2 de la cabecera menos 54 bytes que ocupa esa cabecera
	int imagesize=*(int*)(header+2)-54;

	//Si la imagen tiene tamaño negativo o es superior a 48MB la imagen se rechaza
	if (imagesize<=0|| imagesize > 0x3000000)
		fprintf(stderr, "Imagen muy grande: %d bytes\n", imagesize), exit(1);

	//Si la cabecera no tiene el tamaño de 54 o el numero de bits por pixel es distinto de 24 la imagen se rechaza
	if (*(int*)(header+10)!=54|| *(short*)(header+28)!=24)
		fprintf(stderr, "No color 24-bit\n"), exit(1);

	//Cuando la posicion 30 del header no es 0, es que existe compresion por lo que la imagen no es valida
	if (*(int*)(header+30)!=0)
		fprintf(stderr, "Compresion no suportada\n"), exit(1);

	//Se recupera la altura y anchura de la cabecera
	int width=*(int*)(header+18);
	int height=*(int*)(header+22);
	//**************************************


	// Lectura de la imagen
	//*************************************
	unsigned char *image = (unsigned char*)malloc(imagesize+256+width*6); //Se reservan "imagesize+256+width*6" bytes y se devuelve un puntero a estos datos


	image+=128+width*3;
	if ((n=fread(image, 1, imagesize+1, f))!=imagesize)
		fprintf(stderr, "File size incorrect: %d bytes read insted of %d\n", n, imagesize), exit(1);

	fclose(f);
	printf("Image read correctly (width=%i height=%i, imagesize=%i).\n", width, height, imagesize);

	/* Output variables */
	*w = width;
	*h = height;

	return(image);
}

void writeBMP(float *imageFLOAT, char *file_name, char header[54], int width, int height)
{

	FILE *f;
	int i, n;

	int imagesize=*(int*)(header+2)-54;

	unsigned char *image = (unsigned char*)malloc(3*sizeof(unsigned char)*width*height);

	for (i=0;i<width*height;i++){
		image[3*i]   = imageFLOAT[i]; //R
		image[3*i+1] = imageFLOAT[i]; //G
		image[3*i+2] = imageFLOAT[i]; //B
	}


	f=fopen(file_name, "wb");		//Se abre el fichero en modo binario de escritura
	if (!f){
		perror(file_name);
		exit(1);
	}

	n=fwrite(header, 1, 54, f);		//Primeramente se escribe la cabecera de la imagen
	n+=fwrite(image, 1, imagesize, f);	//Y despues se escribe el resto de la imagen
	if (n!=54+imagesize)			//Si se han escrito diferente cantidad de bytes que la suma de la cabecera y el tamaño de la imagen. Ha habido error
		fprintf(stderr, "Escritos %d de %d bytes\n", n, imagesize+54);
	fclose(f);

	free(image);

}


float *RGB2BW(unsigned char *imageUCHAR, int width, int height)
{
	int i, j;
	float *imageBW = (float *)malloc(sizeof(float)*width*height);

	unsigned char R, B, G;

	for (i=0; i<height; i++)
		for (j=0; j<width; j++)
		{
			R = imageUCHAR[3*(i*width+j)];
			G = imageUCHAR[3*(i*width+j)+1];
			B = imageUCHAR[3*(i*width+j)+2];

			imageBW[i*width+j] = 0.2989 * R + 0.5870 * G + 0.1140 * B;
		}

	return(imageBW);
}

void cannyCPU(float *im, float *image_out,float *NR, float *G, float *phi, float *Gx, float *Gy, int *pedge,float level,int height, int width)
{
	int i, j;
	int ii, jj;
	float PI = 3.141593;

	float lowthres, hithres;

	for(i=2; i<height-2; i++)
		for(j=2; j<width-2; j++)
		{
			// Noise reduction
			NR[i*width+j] =
				 (2.0*im[(i-2)*width+(j-2)] +  4.0*im[(i-2)*width+(j-1)] +  5.0*im[(i-2)*width+(j)] +  4.0*im[(i-2)*width+(j+1)] + 2.0*im[(i-2)*width+(j+2)]
				+ 4.0*im[(i-1)*width+(j-2)] +  9.0*im[(i-1)*width+(j-1)] + 12.0*im[(i-1)*width+(j)] +  9.0*im[(i-1)*width+(j+1)] + 4.0*im[(i-1)*width+(j+2)]
				+ 5.0*im[(i  )*width+(j-2)] + 12.0*im[(i  )*width+(j-1)] + 15.0*im[(i  )*width+(j)] + 12.0*im[(i  )*width+(j+1)] + 5.0*im[(i  )*width+(j+2)]
				+ 4.0*im[(i+1)*width+(j-2)] +  9.0*im[(i+1)*width+(j-1)] + 12.0*im[(i+1)*width+(j)] +  9.0*im[(i+1)*width+(j+1)] + 4.0*im[(i+1)*width+(j+2)]
				+ 2.0*im[(i+2)*width+(j-2)] +  4.0*im[(i+2)*width+(j-1)] +  5.0*im[(i+2)*width+(j)] +  4.0*im[(i+2)*width+(j+1)] + 2.0*im[(i+2)*width+(j+2)])
				/159.0;
		}


	for(i=2; i<height-2; i++)
		for(j=2; j<width-2; j++)
		{
			// Intensity gradient of the image
			Gx[i*width+j] =
				 (1.0*NR[(i-2)*width+(j-2)] +  2.0*NR[(i-2)*width+(j-1)] +  (-2.0)*NR[(i-2)*width+(j+1)] + (-1.0)*NR[(i-2)*width+(j+2)]
				+ 4.0*NR[(i-1)*width+(j-2)] +  8.0*NR[(i-1)*width+(j-1)] +  (-8.0)*NR[(i-1)*width+(j+1)] + (-4.0)*NR[(i-1)*width+(j+2)]
				+ 6.0*NR[(i  )*width+(j-2)] + 12.0*NR[(i  )*width+(j-1)] + (-12.0)*NR[(i  )*width+(j+1)] + (-6.0)*NR[(i  )*width+(j+2)]
				+ 4.0*NR[(i+1)*width+(j-2)] +  8.0*NR[(i+1)*width+(j-1)] +  (-8.0)*NR[(i+1)*width+(j+1)] + (-4.0)*NR[(i+1)*width+(j+2)]
				+ 1.0*NR[(i+2)*width+(j-2)] +  2.0*NR[(i+2)*width+(j-1)] +  (-2.0)*NR[(i+2)*width+(j+1)] + (-1.0)*NR[(i+2)*width+(j+2)]);


			Gy[i*width+j] =
				 ((-1.0)*NR[(i-2)*width+(j-2)] + (-4.0)*NR[(i-2)*width+(j-1)] +  (-6.0)*NR[(i-2)*width+(j)] + (-4.0)*NR[(i-2)*width+(j+1)] + (-1.0)*NR[(i-2)*width+(j+2)]
				+ (-2.0)*NR[(i-1)*width+(j-2)] + (-8.0)*NR[(i-1)*width+(j-1)] + (-12.0)*NR[(i-1)*width+(j)] + (-8.0)*NR[(i-1)*width+(j+1)] + (-2.0)*NR[(i-1)*width+(j+2)]
				+    2.0*NR[(i+1)*width+(j-2)] +    8.0*NR[(i+1)*width+(j-1)] +    12.0*NR[(i+1)*width+(j)] +    8.0*NR[(i+1)*width+(j+1)] +    2.0*NR[(i+1)*width+(j+2)]
				+    1.0*NR[(i+2)*width+(j-2)] +    4.0*NR[(i+2)*width+(j-1)] +     6.0*NR[(i+2)*width+(j)] +    4.0*NR[(i+2)*width+(j+1)] +    1.0*NR[(i+2)*width+(j+2)]);

			G[i*width+j]   = sqrtf((Gx[i*width+j]*Gx[i*width+j])+(Gy[i*width+j]*Gy[i*width+j]));	//G = √Gx²+Gy²
			phi[i*width+j] = atan2f(fabs(Gy[i*width+j]),fabs(Gx[i*width+j]));

			if(fabs(phi[i*width+j])<=PI/8 )
				phi[i*width+j] = 0;
			else if (fabs(phi[i*width+j])<= 3*(PI/8))
				phi[i*width+j] = 45;
			else if (fabs(phi[i*width+j]) <= 5*(PI/8))
				phi[i*width+j] = 90;
			else if (fabs(phi[i*width+j]) <= 7*(PI/8))
				phi[i*width+j] = 135;
			else phi[i*width+j] = 0;
	}

	// Edge
	for(i=3; i<height-3; i++)
		for(j=3; j<width-3; j++)
		{
			pedge[i*width+j] = 0;
			if(phi[i*width+j] == 0){
				if(G[i*width+j]>G[i*width+j+1] && G[i*width+j]>G[i*width+j-1]) //edge is in N-S
					pedge[i*width+j] = 1;

			} else if(phi[i*width+j] == 45) {
				if(G[i*width+j]>G[(i+1)*width+j+1] && G[i*width+j]>G[(i-1)*width+j-1]) // edge is in NW-SE
					pedge[i*width+j] = 1;

			} else if(phi[i*width+j] == 90) {
				if(G[i*width+j]>G[(i+1)*width+j] && G[i*width+j]>G[(i-1)*width+j]) //edge is in E-W
					pedge[i*width+j] = 1;

			} else if(phi[i*width+j] == 135) {
				if(G[i*width+j]>G[(i+1)*width+j-1] && G[i*width+j]>G[(i-1)*width+j+1]) // edge is in NE-SW
					pedge[i*width+j] = 1;
			}
		}

	// Hysteresis Thresholding
	lowthres = level/2;
	hithres  = 2*(level);

	for(i=3; i<height-3; i++)
		for(j=3; j<width-3; j++)
		{
			if(G[i*width+j]>hithres && pedge[i*width+j])
				image_out[i*width+j] = 255;
			else if(pedge[i*width+j] && G[i*width+j]>=lowthres && G[i*width+j]<hithres)
				// check neighbours 3x3
				for (ii=-1;ii<=1; ii++)
					for (jj=-1;jj<=1; jj++)
						if (G[(i+ii)*width+j+jj]>hithres)
							image_out[i*width+j] = 255;
		}
	

}

__global__ void noiseReduction1d(float *d_NR, float *d_im, int height, int width)
{

	int j;
	j = blockIdx.x * blockDim.x + threadIdx.x;

	if (j>=2 && j < width-2)
		for (int i=2;i<height-2;i++){

					d_NR[i*width+j] =  	(2.0*d_im[(i-2)*width+(j-2)] +  4.0*d_im[(i-2)*width+(j-1)] +  5.0*d_im[(i-2)*width+(j)] +  4.0*d_im[(i-2)*width+(j+1)] + 2.0*d_im[(i-2)*width+(j+2)]
									+ 4.0*d_im[(i-1)*width+(j-2)] +  9.0*d_im[(i-1)*width+(j-1)] + 12.0*d_im[(i-1)*width+(j)] +  9.0*d_im[(i-1)*width+(j+1)] + 4.0*d_im[(i-1)*width+(j+2)]
									+ 5.0*d_im[(i  )*width+(j-2)] + 12.0*d_im[(i  )*width+(j-1)] + 15.0*d_im[(i  )*width+(j)] + 12.0*d_im[(i  )*width+(j+1)] + 5.0*d_im[(i  )*width+(j+2)]
									+ 4.0*d_im[(i+1)*width+(j-2)] +  9.0*d_im[(i+1)*width+(j-1)] + 12.0*d_im[(i+1)*width+(j)] +  9.0*d_im[(i+1)*width+(j+1)] + 4.0*d_im[(i+1)*width+(j+2)]
									+ 2.0*d_im[(i+2)*width+(j-2)] +  4.0*d_im[(i+2)*width+(j-1)] +  5.0*d_im[(i+2)*width+(j)] +  4.0*d_im[(i+2)*width+(j+1)] + 2.0*d_im[(i+2)*width+(j+2)])
									/159.0;
	}




}


__global__ void intensityGradienteIm1d(float *d_G, float *d_Gx, float *d_Gy ,float *d_phi ,float *d_NR ,int height, int width)
{
	int j;
	float PI = 3.141593;
	j = blockIdx.x * blockDim.x + threadIdx.x;



	if (j>=2 && j < width-2)
		for (int i=2;i<height-2;i++){
				// Intensity gradient of the image
			d_Gx[i*width+j] =
					 (1.0*d_NR[(i-2)*width+(j-2)] +  2.0*d_NR[(i-2)*width+(j-1)] +  (-2.0)*d_NR[(i-2)*width+(j+1)] + (-1.0)*d_NR[(i-2)*width+(j+2)]
					+ 4.0*d_NR[(i-1)*width+(j-2)] +  8.0*d_NR[(i-1)*width+(j-1)] +  (-8.0)*d_NR[(i-1)*width+(j+1)] + (-4.0)*d_NR[(i-1)*width+(j+2)]
					+ 6.0*d_NR[(i  )*width+(j-2)] + 12.0*d_NR[(i  )*width+(j-1)] + (-12.0)*d_NR[(i  )*width+(j+1)] + (-6.0)*d_NR[(i  )*width+(j+2)]
					+ 4.0*d_NR[(i+1)*width+(j-2)] +  8.0*d_NR[(i+1)*width+(j-1)] +  (-8.0)*d_NR[(i+1)*width+(j+1)] + (-4.0)*d_NR[(i+1)*width+(j+2)]
					+ 1.0*d_NR[(i+2)*width+(j-2)] +  2.0*d_NR[(i+2)*width+(j-1)] +  (-2.0)*d_NR[(i+2)*width+(j+1)] + (-1.0)*d_NR[(i+2)*width+(j+2)]);


			d_Gy[i*width+j] =
					 ((-1.0)*d_NR[(i-2)*width+(j-2)] + (-4.0)*d_NR[(i-2)*width+(j-1)] +  (-6.0)*d_NR[(i-2)*width+(j)] + (-4.0)*d_NR[(i-2)*width+(j+1)] + (-1.0)*d_NR[(i-2)*width+(j+2)]
					+ (-2.0)*d_NR[(i-1)*width+(j-2)] + (-8.0)*d_NR[(i-1)*width+(j-1)] + (-12.0)*d_NR[(i-1)*width+(j)] + (-8.0)*d_NR[(i-1)*width+(j+1)] + (-2.0)*d_NR[(i-1)*width+(j+2)]
					+    2.0*d_NR[(i+1)*width+(j-2)] +    8.0*d_NR[(i+1)*width+(j-1)] +    12.0*d_NR[(i+1)*width+(j)] +    8.0*d_NR[(i+1)*width+(j+1)] +    2.0*d_NR[(i+1)*width+(j+2)]
					+    1.0*d_NR[(i+2)*width+(j-2)] +    4.0*d_NR[(i+2)*width+(j-1)] +     6.0*d_NR[(i+2)*width+(j)] +    4.0*d_NR[(i+2)*width+(j+1)] +    1.0*d_NR[(i+2)*width+(j+2)]);


			 __syncthreads();
			d_G[i*width+j]   = sqrtf((d_Gx[i*width+j]*d_Gx[i*width+j])+(d_Gy[i*width+j]*d_Gy[i*width+j]));	//G = √Gx²+Gy²
			d_phi[i*width+j] = atan2f(fabs(d_Gy[i*width+j]),fabs(d_Gx[i*width+j]));
			 __syncthreads();
			 	if(fabs(d_phi[i*width+j])<=PI/8 )
					d_phi[i*width+j] = 0;
				else if (fabs(d_phi[i*width+j])<= 3*(PI/8))
					d_phi[i*width+j] = 45;
				else if (fabs(d_phi[i*width+j]) <= 5*(PI/8))
					d_phi[i*width+j] = 90;
				else if (fabs(d_phi[i*width+j]) <= 7*(PI/8))
					d_phi[i*width+j] = 135;
				else d_phi[i*width+j] = 0;
		}


}

__global__ void edges1d(int *d_pedge, float *d_G,float *d_phi, int height, int width)
{
	int j;
	j = blockIdx.x * blockDim.x + threadIdx.x;


	// Edge
	if (j>=2 && j < width-2)
		for (int i=2;i<height-2;i++){
				d_pedge[i*width+j] = 0;
				if(d_phi[i*width+j] == 0){
					if(d_G[i*width+j]>d_G[i*width+j+1] && d_G[i*width+j]>d_G[i*width+j-1]) //edge is in N-S
						d_pedge[i*width+j] = 1;

				} else if(d_phi[i*width+j] == 45) {
					if(d_G[i*width+j]>d_G[(i+1)*width+j+1] && d_G[i*width+j]>d_G[(i-1)*width+j-1]) // edge is in NW-SE
						d_pedge[i*width+j] = 1;

				} else if(d_phi[i*width+j] == 90) {
					if(d_G[i*width+j]>d_G[(i+1)*width+j] && d_G[i*width+j]>d_G[(i-1)*width+j]) //edge is in E-W
						d_pedge[i*width+j] = 1;

				} else if(d_phi[i*width+j] == 135) {
					if(d_G[i*width+j]>d_G[(i+1)*width+j-1] && d_G[i*width+j]>d_G[(i-1)*width+j+1]) // edge is in NE-SW
						d_pedge[i*width+j] = 1;
				}
			}


}

__global__ void hysteresisThresholding1d(float *d_image_out, int level ,int* d_pedge,float *d_G, int height, int width)
{

	int j;
	j = blockIdx.x * blockDim.x + threadIdx.x;


	float lowthres,hithres;

// Hysteresis Thresholding
	lowthres = level/2;
	hithres  = 2*(level);
	if (j<width)
		for (int i=0;i<height;i++)
			d_image_out[i*width+j]=0.0; //Inicializamos a 0
	 __syncthreads();

	 if (j>=3 && j < width-3)
		 for (int i=3;i<height-3;i++){
			if(d_G[i*width+j]>hithres && d_pedge[i*width+j])
				d_image_out[i*width+j] = 255;
			else if(d_pedge[i*width+j] && d_G[i*width+j]>=lowthres && d_G[i*width+j]<hithres)
				// check neighbours 3x3
				for (int ii=-1;ii<=1; ii++)
					for (int jj=-1;jj<=1; jj++)
						if (d_G[(i+ii)*width+j+jj]>hithres)
							d_image_out[i*width+j] = 255;
		}
}



void cannyGPU1d(float *im,float *image_out,float *d_NR, float *d_G, float *d_phi, float *d_Gx, float *d_Gy, int *d_pedge,
	float level,
	int height, int width)
{



	float * d_im;
	float *d_image_out;

	cudaError_t rc;

	rc=cudaMalloc((void**)&d_im, sizeof(float)*height*width);
	if (rc != cudaSuccess)
		printf("Could not allocate memory: %d", rc);
	rc=cudaMalloc((void**)&d_image_out, sizeof(float)*height*width);
	if (rc != cudaSuccess)
		printf("Could not allocate memory: %d", rc);

	rc=cudaMemcpy(d_im, im, sizeof(float)*height*width, cudaMemcpyHostToDevice);//pasamos la imagen a GPU
	if (rc != cudaSuccess)
		printf("Could not copy image: %d", rc);


	//Parámetros de Grid y BLOCKS
	int N;
	if (height>width)
		N = height;
	else
		N = width;

	dim3 dimBlock(NTHREADS);
	int blocks = N/NTHREADS;
	if (N%NTHREADS>0)
		blocks++;
	dim3 dimGrid(blocks);
	// Launch the device computation

	noiseReduction1d<<<dimBlock, dimGrid>>>(d_NR, d_im, height, width);
	cudaGetErrorString(cudaThreadSynchronize());

	intensityGradienteIm1d<<<dimBlock, dimGrid>>>(d_G,d_Gx,d_Gy,d_phi,d_NR, height, width);
	cudaGetErrorString(cudaThreadSynchronize());


	edges1d<<<dimBlock, dimGrid>>>(d_pedge,d_G,d_phi, height, width);
	cudaGetErrorString(cudaThreadSynchronize());


	hysteresisThresholding1d<<<dimBlock, dimGrid>>>(d_image_out,level,d_pedge,d_G, height, width);
	cudaGetErrorString(cudaThreadSynchronize());


	cudaMemcpy(image_out, d_image_out, sizeof(float)*height*width, cudaMemcpyDeviceToHost);//pasamos la imagen a GPU
	cudaFree(d_im);
	cudaFree(d_image_out);

}

void freeMemory(unsigned char *imageUCHAR, float *imageBW, float *NR, float *G, float *phi, float *Gx, float *Gy, int *pedge, float *imageOUT)
{
	//free(imageUCHAR);
	free(imageBW);
	free(imageOUT);
	free(NR);
	free(G);
	free(phi);
	free(Gx);
	free(Gy);
	free(pedge);
}


int main(int argc, char **argv) {

	int width, height;
	unsigned char *imageUCHAR;
	float *imageBW;

	char header[54];


	//Variables para calcular el tiempo
	double t0, t1;

	//Tener menos de 3 argumentos es incorrecto
	if (argc < 3) {
		fprintf(stderr, "Uso incorrecto de los parametros. exe  input.bmp output.bmp \n");
		exit(1);
	}


	// READ IMAGE & Convert image
	imageUCHAR = readBMP(argv[1], header, &width, &height);
	imageBW = RGB2BW(imageUCHAR, width, height);



	// Aux. memory
	float *NR       = (float *)malloc(sizeof(float)*width*height);
	float *G        = (float *)malloc(sizeof(float)*width*height);
	float *phi      = (float *)malloc(sizeof(float)*width*height);
	float *Gx       = (float *)malloc(sizeof(float)*width*height);
	float *Gy       = (float *)malloc(sizeof(float)*width*height);
	int *pedge      = (int *)malloc(sizeof(int)*width*height);
	float *imageOUTCPU = (float *)malloc(sizeof(float)*width*height);
	float *imageOUT1d = (float *)malloc(sizeof(float)*width*height);
	float *imageOUT2d = (float *)malloc(sizeof(float)*width*height);
	float *imageOUT2dUQ = (float *)malloc(sizeof(float)*width*height);

	//Aux. Memory Device 1d
	float *d_NR;
	float *d_G;
	float *d_phi;
	float *d_Gx;
	float *d_Gy;
	int *d_pedge;


	int cudaSize;
	cudaSize= sizeof(float)*width*height;



	////////////////
	// CANNY      //
	////////////////
	//***********BLOQUE CPU******************//
	t0 = get_time();
	cannyCPU(imageBW, imageOUTCPU, NR, G, phi, Gx, Gy, pedge,1000.0, height, width);
	t1 = get_time();
	double timeCPU = t1-t0;
	printf("CPU Exection time in                       %f ms. \n", t1-t0);
	char bufCPU[256];
	snprintf(bufCPU, sizeof bufCPU, "%s%s", argv[2], "CPU.bmp");
	writeBMP(imageOUTCPU, bufCPU, header, width, height);

	//***********BLOQUE CPU FIN******************//



	//***********BLOQUE 1D******************//
	cudaMalloc((void**)&d_NR, cudaSize);
	cudaMalloc((void**)&d_G, cudaSize);
	cudaMalloc((void**)&d_phi, cudaSize);
	cudaMalloc((void**)&d_Gx, cudaSize);
	cudaMalloc((void**)&d_Gy, cudaSize );
	cudaMalloc((void**)&d_pedge, sizeof(int)*width*height );


	t0 = get_time();
	cannyGPU1d(imageBW,imageOUT1d,d_NR, d_G, d_phi, d_Gx, d_Gy, d_pedge,1000.0, height, width);
	t1 = get_time();
	float tiempo1d = timeCPU/(t1-t0);
	printf("GPU Exection time in 1d                    %f and sppedUP of %f  \n", t1-t0,tiempo1d);
	char buf[256];
	snprintf(buf, sizeof buf, "%s%s", argv[2], "1d.bmp");
	writeBMP(imageOUT1d, buf, header, width, height);

	cudaFree(d_NR);
	cudaFree(d_G);
	cudaFree(d_phi);
	cudaFree(d_Gx);
	cudaFree(d_Gy);
	cudaFree(d_pedge);
	free(imageOUT1d);
	//***********BLOQUE 1D FIN******************//



	cudaDeviceReset();
	freeMemory(imageUCHAR, imageBW, NR, G, phi, Gx, Gy, pedge, imageOUTCPU);


}